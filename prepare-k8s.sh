#!/bin/bash
namespace=$1
kubectl get namespace $namespace
if [ $? -eq 0 ]
then
  echo "namespace $namespace already exists"
else
  echo "namespace $namespace doesn't exist" >&2
  kubectl create namespace $namespace
fi

secret=$2
kubectl -n $namespace get secret $secret
if [ $? -eq 0 ]
then
  echo "secret $secret already exists"
else
  echo "secret $secret doesn't exist" >&2
  kubectl -n $namespace create secret docker-registry $secret --docker-server=https://registry.gitlab.com --docker-username=Uralov --docker-password=$REGISTRY_PASSWD --docker-email=m_uralov@yahoo.com
fi

# creating service account and setting cluster-admin role for tiller because k8s cluster RBAC is enabled
kubectl -n kube-system get sa tiller
if [ $? -eq 0 ]
then
  echo "service account tiller already exists"
else
echo "service account tiller doesn't exist" >&2
password=$(gcloud container clusters describe cluster-1 --zone europe-west3-b --project cnad-academy-asg --format='value(masterAuth.password)')
echo "$password"
cat <<EOF | kubectl --username=admin --password=$password create -f -
apiVersion: v1
kind: ServiceAccount
metadata:
  name: tiller
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: tiller
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
  - kind: ServiceAccount
    name: tiller
    namespace: kube-system
EOF
fi
