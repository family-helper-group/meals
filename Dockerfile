FROM openjdk:8-alpine

MAINTAINER Mansur
ENV VERSION="1.0.0"

RUN mkdir /app
WORKDIR /app
COPY build/libs/meals-$VERSION.jar /app
VOLUME /app

EXPOSE 8080

#the following command waits for the postgres server to start and creats "food" database on postgres container
ENTRYPOINT java -Dspring.profiles.active=prod -jar meals-$VERSION.jar