# Documentation for meals microservice

There is a manual step where food databse in the postgres pod must be created:

$ kubectl -n {namespace} exec -it {pod name} bash

Run the following command inside the container:
$ createdb -p 5432 -h localhost -e food -U postgres -w postgres
