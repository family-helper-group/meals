<h1>meals microservice</h1>
The meals service provides REST resources related to meals. It has meal, food, etc. resources.

<h3>Manual steps:</h3>
The following only manual step must be executed after deploying the app:
Create a "food" database in the postgres pod meals-db container with the following:
   - Get into the container with "kubectl -n family-helper-stage exec {pod name} -i -t -- bash"
   - Run "createdb -p 5432 -h localhost -e food -U postgres -w postgres" to create the database

<h3>Leftovers:</h3>
Currently postrgres deployment has an ephemeral volume type. This means that if the postgres pod is deleted all the data will be lost. For more info refer to https://cloud.google.com/kubernetes-engine/docs/concepts/volumes